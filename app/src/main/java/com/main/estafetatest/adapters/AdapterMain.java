package com.main.estafetatest.adapters;


import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.main.estafetatest.R;
import com.main.estafetatest.data.SurveyTask;
import com.main.estafetatest.databinding.RvMain;

import java.util.List;


public class AdapterMain extends RecyclerView.Adapter<AdapterMain.MainViewHolder> implements View.OnClickListener {
    private List<SurveyTask> list;
    private RvMain binder;
    private ItemClickListener listener;


    public AdapterMain(List<SurveyTask> list, ItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        binder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_main_list, parent, false);
        return new MainViewHolder(binder.getRoot());
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        binder.setTask(list.get(position));
        binder.container.setOnClickListener(this);
        binder.container.setTag(list.get(position).getId());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onClick(View v) {
        listener.onItemClickListener(v);
    }

    public interface ItemClickListener{
        void onItemClickListener(View v);
    }

    public class MainViewHolder extends RecyclerView.ViewHolder{
        public MainViewHolder(View itemView) {
            super(itemView);
            DataBindingUtil.bind(itemView);
        }
    }
}
