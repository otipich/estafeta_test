package com.main.estafetatest.data;


import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SurveyTask extends CommonData implements Serializable {

    @SerializedName("Id")
    @Expose
    private int id;
    @SerializedName("Number")
    @Expose
    private String number;
    @SerializedName("PlannedStartDate")
    @Expose
    private String plannedStartDate;
    @SerializedName("PlannedEndDate")
    @Expose
    private String plannedEndDate;
    @SerializedName("ActualStartDate")
    @Expose
    private String actualStartDate;
    @SerializedName("ActualEndDate")
    @Expose
    private String actualEndDate;
    @SerializedName("Vin")
    @Expose
    private String vin;
    @SerializedName("Model")
    @Expose
    private String model;
    @SerializedName("ModelCode")
    @Expose
    private String modelCode;
    @SerializedName("Brand")
    @Expose
    private String brand;
    @SerializedName("SurveyPoint")
    @Expose
    private String surveyPoint;
    @SerializedName("Carrier")
    @Expose
    private String carrier;
    @SerializedName("Driver")
    @Expose
    private String driver;

    public SurveyTask(int id, String number, String plannedStartDate, String plannedEndDate, String actualStartDate, String actualEndDate, String vin, String model, String modelCode, String brand, String surveyPoint, String carrier, String driver) {
        this.id = id;
        this.number = number;
        this.plannedStartDate = plannedStartDate;
        this.plannedEndDate = plannedEndDate;
        this.actualStartDate = actualStartDate;
        this.actualEndDate = actualEndDate;
        this.vin = vin;
        this.model = model;
        this.modelCode = modelCode;
        this.brand = brand;
        this.surveyPoint = surveyPoint;
        this.carrier = carrier;
        this.driver = driver;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Bindable
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Bindable
    public String getPlannedStartDate() {
        return plannedStartDate;
    }

    public void setPlannedStartDate(String plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    @Bindable
    public String getPlannedEndDate() {
        return plannedEndDate;
    }

    public void setPlannedEndDate(String plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    @Bindable
    public String getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(String actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    @Bindable
    public String getActualEndDate() {
        return actualEndDate;
    }

    public void setActualEndDate(String actualEndDate) {
        this.actualEndDate = actualEndDate;
    }

    @Bindable
    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Bindable
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Bindable
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @Bindable
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Bindable
    public String getSurveyPoint() {
        return surveyPoint;
    }

    public void setSurveyPoint(String surveyPoint) {
        this.surveyPoint = surveyPoint;
    }

    @Bindable
    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    @Bindable
    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }
}
