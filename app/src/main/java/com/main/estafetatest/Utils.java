package com.main.estafetatest;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;

import com.main.estafetatest.core.App;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static String getBase64Credentials(String login, String pass){
        String basicAuth = login+":"+pass;
        String base64EncodedCredentials = Base64.encodeToString(basicAuth.getBytes(), Base64.NO_WRAP);
        return "Basic "+ base64EncodedCredentials;
    }

    public static boolean hasConnection() {
        InternetStatus status = getInternetStatus(App.getContext());
        return status == InternetStatus.HAVE_GPRS || status == InternetStatus.HAVE_WIFI;
    }

    public static String getString(int id){
        return App.getContext().getResources().getString(id);
    }

    public static String getFormattedTime(String time) {
        String pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date != null ? date.toString() : null;
    }

    private static InternetStatus getInternetStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) return InternetStatus.NO_INTERNET;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        if (netInfo == null) return InternetStatus.NO_INTERNET;
        for (NetworkInfo ni : netInfo) {
            if ("wifi".equalsIgnoreCase(ni.getTypeName()) && ni.isConnected())
                return InternetStatus.HAVE_WIFI;
            else if ("mobile".equalsIgnoreCase(ni.getTypeName()) && ni.isConnected())
                return InternetStatus.HAVE_GPRS;
        }
        return InternetStatus.NO_INTERNET;
    }

    public enum InternetStatus {
        NO_INTERNET,
        HAVE_GPRS,
        HAVE_WIFI,
    }
}
