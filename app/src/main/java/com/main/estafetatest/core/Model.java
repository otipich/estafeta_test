package com.main.estafetatest.core;


import com.main.estafetatest.data.SurveyTask;

import java.util.List;

public class Model {
    private static Model instance = new Model();
    private List<SurveyTask> surveyTaskList;
    public static Model getInstance() {
        return instance;
    }


    public List<SurveyTask> getSurveyTaskList() {
        return surveyTaskList;
    }

    public void setSurveyTaskList(List<SurveyTask> surveyTaskList) {
        this.surveyTaskList = surveyTaskList;
    }

    public SurveyTask findTaskById(int id){
        for (SurveyTask task: getSurveyTaskList()){
            if (id == task.getId())
                return task;
        }
        return null;
    }
}
