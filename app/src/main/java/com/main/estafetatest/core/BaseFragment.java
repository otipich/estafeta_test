package com.main.estafetatest.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;


public class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public ActMain getActMain() {
        return (ActMain) getActivity();
    }

    public void finish() {
        getFragmentManager().popBackStack();
    }

    public void setFragmentTitle(String title) {
        getActMain().setTitle(title);
    }

    public LinearLayoutManager getLinearLayoutManager(){
        return new LinearLayoutManager(App.getContext());
    }

    public void showToast(String text){
        Toast.makeText(App.getContext(),text,Toast.LENGTH_LONG).show();
    }

}
