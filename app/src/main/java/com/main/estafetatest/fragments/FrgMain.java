package com.main.estafetatest.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.main.estafetatest.R;
import com.main.estafetatest.Utils;
import com.main.estafetatest.adapters.AdapterMain;
import com.main.estafetatest.core.App;
import com.main.estafetatest.core.BaseFragment;
import com.main.estafetatest.core.Model;
import com.main.estafetatest.data.SurveyTask;
import com.main.estafetatest.databinding.FrgMainBinding;
import com.main.estafetatest.server.IRequestExecutor;
import com.main.estafetatest.server.Server;
import com.main.estafetatest.server.request.ICallback;

import java.util.List;


public class FrgMain extends BaseFragment implements ICallback, AdapterMain.ItemClickListener {
    private FrgMainBinding binding;
    private boolean isNeedToGetTasks = true;

    public static FrgMain newInstance() {
        return new FrgMain();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frg_main, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (!Utils.hasConnection()){
            showToast(Utils.getString(R.string.no_internet));
            return;
        }
        if (isNeedToGetTasks) {
            IRequestExecutor executor = new Server();
            executor.getAllTasks(this);
        } else {
            binding.rv.setLayoutManager(getLinearLayoutManager());
            binding.rv.setAdapter(new AdapterMain(Model.getInstance().getSurveyTaskList(), this));
        }
    }

    @Override
    public void onResponseResult(List commonData) {
        if (commonData.get(0).getClass().equals(SurveyTask.class)) {
            List<SurveyTask> response = (List<SurveyTask>) commonData;
            Model.getInstance().setSurveyTaskList(response);
            binding.rv.setLayoutManager(getLinearLayoutManager());
            binding.rv.setAdapter(new AdapterMain(response, this));
            isNeedToGetTasks = false;
        }
    }

    @Override
    public void onItemClickListener(View v) {
        int id = (int) v.getTag();
        if (id > 0) {
            SurveyTask task = Model.getInstance().findTaskById(id);
            getActMain().showFragment(FrgTask.newInstance(task), true);
        }
    }
}
