package com.main.estafetatest.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.main.estafetatest.R;
import com.main.estafetatest.core.BaseFragment;
import com.main.estafetatest.data.SurveyTask;
import com.main.estafetatest.databinding.FrgTaskBinding;

public class FrgTask extends BaseFragment {
    private static final String TASK_KEY = "task_key";
    private FrgTaskBinding binding;

    public static FrgTask newInstance(SurveyTask task) {
        Bundle args = new Bundle();
        args.putSerializable(TASK_KEY, task);
        FrgTask fragment = new FrgTask();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.frg_task, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setTask((SurveyTask) getArguments().getSerializable(TASK_KEY));
    }
}
