package com.main.estafetatest.server.request;


import com.main.estafetatest.data.SurveyTask;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface IGetSurveyTasksRequest {

    @GET("api/mobilesurveytasks/gettestsurveytasks ")
    Call<List<SurveyTask>> getTasks(@Header("Authorization") String base64Auth);
}
