package com.main.estafetatest.server;

import com.main.estafetatest.Utils;
import com.main.estafetatest.data.SurveyTask;
import com.main.estafetatest.server.request.ICallback;
import com.main.estafetatest.server.request.IGetSurveyTasksRequest;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Server implements IRequestExecutor {

    private Retrofit getRetrofitBuilder() {
        return new Retrofit.Builder()
                .baseUrl("http://amt2.estafeta.org/")
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    @Override
    public void getAllTasks(final ICallback iCallback) {
        IGetSurveyTasksRequest service = getRetrofitBuilder().create(IGetSurveyTasksRequest.class);
        Call<List<SurveyTask>> call = service.getTasks(Utils.getBase64Credentials("admin","1"));
        call.enqueue(new Callback<List<SurveyTask>>() {

            @Override
            public void onResponse(Call<List<SurveyTask>> call, Response<List<SurveyTask>> response) {
                iCallback.onResponseResult(response.body());
            }

            @Override
            public void onFailure(Call<List<SurveyTask>> call, Throwable t) {
                t.fillInStackTrace();
            }
        });
    }
}
