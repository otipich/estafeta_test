package com.main.estafetatest.server.request;


import com.main.estafetatest.data.CommonData;

import java.util.List;

public interface ICallback <T extends CommonData> {
    void onResponseResult(List<T> commonData);
}