package com.main.estafetatest.server;


import com.main.estafetatest.server.request.ICallback;

public interface IRequestExecutor {
    void getAllTasks(ICallback iCallback);
}
